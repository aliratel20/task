import './App.css';
import NavBar from './components/NavBar';
import Home from './components/Home';
import NewDomain from './components/NewDomain';
import Packages from './components/Packages';
import About from './components/About';
import Slider from './components/Slider/Slider.js';
import Pro from './components/Pro';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <div className='Header'>
        <NavBar />
        <Home />
      </div>
      <NewDomain />
      <Packages />
      <About />
      <Slider />
      <Pro />
      <Footer />
    </div>
  );
}

export default App;
