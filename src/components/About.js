import React from 'react'
import '../css/About.css'
import av1 from '../img/avatar-1.jpg'
import av2 from '../img/avatar-2.jpg'
import av3 from '../img/avatar-3.jpg'

function About() {
    return (
        <div className='about'>
            <div className='text'>
                <h1>About</h1>
                <article>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Suscipit unde magni aut voluptates dicta facilis assumenda quae
                    praesentium neque, enim quia quisquam eaque nesciunt.
                    Adipisci tenetur non dolorum exercitationem quos.
                </article>
            </div>
            <div className='team'>
                <div className='one'>
                    <img src={av1} alt="" />
                    <h2>Jonny Doe</h2>
                    <p>Designer</p>
                </div>
                <div className='two'>
                    <img src={av2} alt="" />
                    <h2>Jonny Doe</h2>
                    <p>Designer</p>
                </div>
                <div className='three'>
                    <img src={av3} alt="" />
                    <h2>Jonny Doe</h2>
                    <p>Designer</p>
                </div>
            </div>
        </div>
    )
}

export default About