import React from 'react'
import '../css/Footer.css'

function Footer() {
    return (
        <footer className="footer">
            <div className="container">
                <div className="row">
                    <div className='NAV'>
                        <nav>
                            <ul class="nav__link">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Projects</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#">About</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div className="row">
                    <div className="footer-col lorem">
                        <ul>
                            <li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Cupiditate culpa tempore, provident molestias magnam beatae quisquam magni ducimus, corrupti, minus ipsa esse non accusantium laudantium repudiandae nobis temporibus quis laborum?</li>
                        </ul>
                    </div>
                    <div className="footer-col">
                        <ul>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Account</a></li>
                            <li><a href="#">Product Catalog</a></li>
                            <li><a href="#">Shortcodes</a></li>
                        </ul>
                    </div>
                    <div className="footer-col">
                        <ul>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Account</a></li>
                            <li><a href="#">Product Catalog</a></li>
                            <li><a href="#">Shortcodes</a></li>
                        </ul>
                    </div>
                    <div className="footer-col">
                        <ul>
                            <li><a href="#">0000-0000-0000</a></li>
                            <li><a href="#">aliratel20@gmail.com</a></li>
                            <li><a href="#">Lorem ipsum dolor sit amet consectetur</a></li>
                            <li><a href="#">Ipsum</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="aliratel">
                    made by AliRatel with pure css , 2022
            </div>
        </footer>
    )
}

export default Footer