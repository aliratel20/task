import React from 'react'
import '../css/Home.css'
function Home() {
  return (
    <div>
      <section className='home'>
        <div className='content'>
          <h1>HELLO!</h1>
          <p>We love our work</p>
          <p>And we're taking it seriously</p>
        </div>
        <div className='Reg'>
          <div className='log'>
            <div className='login'>login</div>
          </div>
          <div className='regis'>
            <div className='register'>register</div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default Home