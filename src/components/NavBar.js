import React from 'react'
import '../css/NavBar.css'

function NavBar() {
    window.addEventListener("scroll", function() {
        var header = document.querySelector("header");
        header.classList.toggle("sticky", window.scrollY > 0);
      });      
    return (
        <header>
            <svg class="logo" xmlns="http://www.w3.org/2000/svg" width="256" height="256" viewBox="0 0 512 512" fill="#fff">
                <g transform="translate(512.001 0) rotate(90)">
                    <text transform="translate(356.426 -35.119) rotate(-90)" font-size="280" font-family="Montserrat-SemiBold, Montserrat" font-weight="600">
                        <tspan x="0" y="0">Host Gost</tspan>
                    </text>
                </g>
            </svg>
            <nav>
                <ul class="nav__link">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">About</a></li>
                </ul>
            </nav>
        </header>
    )
}

export default NavBar