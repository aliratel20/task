import React from 'react'
import '../css/packages.css'
import yellow from '../img/yellow.png';
import red from '../img/red.png';
import gray from '../img/gray.png';
import green from '../img/green.png';
function Packages() {
    return (
        <div className="price-table-wrapper">
            <div className="pricing-table basic">
                <div id="element">
                    <div class="inner">
                        <img src={yellow} width='30vmin' alt="yello" />
                    </div>
                </div>
                <p className='package'>basic package</p>
                <hr />
                <ul className="pricing-table__list">
                    <tr>
                        <th>Disck Space(GB)</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Subdomain</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Transfer(GB)</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Data bases</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th>Dashboards</th>
                    </tr>
                    <tr>
                        <th>Control Panel &FTP</th>
                    </tr>
                    <tr>
                        <th>Free Support</th>
                    </tr>
                </ul>
                
                <hr />
                <div className='plan'>
                    <h3><span id='dollar'>$</span>20<span id='mo'>/mo</span></h3>
                    <h2><a href="/">SELECT PLAN</a></h2>
                </div>
            </div>
            <div className="pricing-table normal">
                <div id="element">
                    <div class="inner">
                        <img src={gray} width='30vmin' alt="yello" />
                    </div>
                </div>
                <p>normal package</p>
                <hr />
                <ul className="pricing-table__list">
                    <tr>
                        <th>Disck Space(GB)</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Subdomain</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Transfer(GB)</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Data bases</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th>Dashboards</th>
                    </tr>
                    <tr>
                        <th>Control Panel &FTP</th>
                    </tr>
                    <tr>
                        <th>Free Support</th>
                    </tr>
                </ul>
                
                <hr />
                <div className='plan'>
                    <h3><span id='dollar'>$</span>20<span id='mo'>/mo</span></h3>
                    <h2><a href="/">SELECT PLAN</a></h2>
                </div>
            </div>
            <div className="pricing-table big">
                <div id="element">
                    <div class="inner">
                        <img src={red} width='30vmin' alt="yello" />
                    </div>
                </div>
                <p>big package</p>
                <hr />
                <ul className="pricing-table__list">
                    <tr>
                        <th>Disck Space(GB)</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Subdomain</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Transfer(GB)</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Data bases</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th>Dashboards</th>
                    </tr>
                    <tr>
                        <th>Control Panel &FTP</th>
                    </tr>
                    <tr>
                        <th>Free Support</th>
                    </tr>
                </ul>
                <hr />
                <div className='plan'>
                    <h3><span id='dollar'>$</span>20<span id='mo'>/mo</span></h3>
                    <h2><a href="/">SELECT PLAN</a></h2>
                </div>
            </div>
            <div className="pricing-table bigest">
                <div id="element">
                    <div class="inner">
                        <img src={green} width='30vmin' alt="yello" />
                    </div>
                </div>
                <p>bigest package</p>
                <hr />
                <ul className="pricing-table__list">
                    <tr>
                        <th>Disck Space(GB)</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Subdomain</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Transfer(GB)</th>
                        <td>50</td>
                    </tr>
                    <tr>
                        <th>Data bases</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th>Dashboards</th>
                    </tr>
                    <tr>
                        <th>Control Panel &FTP</th>
                    </tr>
                    <tr>
                        <th>Free Support</th>
                    </tr>
                </ul>
                <hr />
                <div className='plan'>
                    <h3><span id='dollar'>$</span>20<span id='mo'>/mo</span></h3>
                    <h2><a href="/">SELECT PLAN</a></h2>
                </div>
            </div>
        </div>
    )
}

export default Packages